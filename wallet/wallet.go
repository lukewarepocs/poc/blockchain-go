package main

//https://www.sohamkamani.com/golang/rsa-encryption/#:~:text=The%20public%20and%20private%20keys,by%20it's%20corresponding%20public%20key.
import (
  "crypto"
  crand "crypto/rand"
  "crypto/rsa"
  "crypto/sha256"
  "crypto/sha512"
  "crypto/x509"
  "encoding/pem"
  "fmt"
  "math/rand"
  "strings"
)

// Wallet represents a wallet with a balance and an address.
type Wallet struct {
  balance float64
  address string
}

// NewWallet creates a new wallet with a random address and a balance of 0.
func NewWallet() *Wallet {
  return &Wallet{
    address: randAddress(),
    balance: 0.0,
  }
}

func randAddress() string {
  b := make([]byte, 32)
  _, _ = rand.Read(b)
  return fmt.Sprintf("%x", b)
}

// Deposit adds funds to the wallet's balance.
func (w *Wallet) Deposit(amount float64) {
  w.balance += amount
}

// Withdraw removes funds from the wallet's balance.
func (w *Wallet) Withdraw(amount float64) {
  w.balance -= amount
}

// Balance returns the current balance of the wallet.
func (w *Wallet) Balance() float64 {
  return w.balance
}

func main() {

  privateKey, err := rsa.GenerateKey(crand.Reader, 4096)
  if err != nil {
    panic(err)
  }

  fmt.Println(privateKey)

  publicKey := privateKey.PublicKey

  publicKey2 := privateKey.Public()

  fmt.Println(publicKey)

  keyPEM := pem.EncodeToMemory(
    &pem.Block{
      Type:  "RSA PRIVATE KEY",
      Bytes: x509.MarshalPKCS1PrivateKey(privateKey),
    },
  )

  pubPEM := pem.EncodeToMemory(
    &pem.Block{
      Type:  "RSA PUBLIC KEY",
      Bytes: x509.MarshalPKCS1PublicKey(publicKey2.(*rsa.PublicKey)),
    },
  )

  keyPEMStr := convert(keyPEM)
  pubPEMStr := convert(pubPEM)

  fmt.Println(keyPEMStr)
  fmt.Println(pubPEMStr)

  encryptedBytes, err := rsa.EncryptOAEP(
    sha512.New(),
    crand.Reader,
    &publicKey,
    []byte("super secret message"),
    nil)

  encryptedBytesStr := convert(encryptedBytes)
  fmt.Println(encryptedBytesStr)

  if err != nil {
    panic(err)
  }

  fmt.Println("encrypted bytes: ", encryptedBytes)

  decryptedBytes, err := privateKey.Decrypt(nil, encryptedBytes, &rsa.OAEPOptions{Hash: crypto.SHA512})
  if err != nil {
    panic(err)
  }

  decryptedBytesStr := convert(decryptedBytes)
  fmt.Println(decryptedBytesStr)
  // We get back the original information in the form of bytes, which we
  // the cast to a string and print
  fmt.Println("decrypted message: ", string(decryptedBytes))

  msg := []byte("verifiable message")

  // Before signing, we need to hash our message
  // The hash is what we actually sign
  msgHash := sha256.New()
  _, err = msgHash.Write(msg)
  if err != nil {
    panic(err)
  }
  msgHashSum := msgHash.Sum(nil)

  // In order to generate the signature, we provide a random number generator,
  // our private key, the hashing algorithm that we used, and the hash sum
  // of our message
  signature, err := rsa.SignPSS(crand.Reader, privateKey, crypto.SHA256, msgHashSum, nil)
  if err != nil {
    panic(err)
  }

  // To verify the signature, we provide the public key, the hashing algorithm
  // the hash sum of our message and the signature we generated previously
  // there is an optional "options" parameter which can omit for now
  err = rsa.VerifyPSS(&publicKey, crypto.SHA256, msgHashSum, signature, nil)
  if err != nil {
    fmt.Println("could not verify signature: ", err)
    return
  }
  // If we don't get any error from the `VerifyPSS` method, that means our
  // signature is valid
  fmt.Println("signature verified")

  w := NewWallet()
  fmt.Println("Address: ", w.address, "\nBalanco inicial: ", w.balance)
  fmt.Println()
  w.Deposit(100)
  fmt.Println("Deposito realizar no address: ", w.address, "\nBalanco: ", w.balance)
  fmt.Println()
  w.Withdraw(50.0090879998876787)
  fmt.Println("Retirada realizar no address: ", w.address, "\nBalanco: ", w.balance)
}

func convert(b []byte) string {
  s := make([]string, len(b))
  for i := range b {
    s[i] = string(b[i])
  }
  return strings.Join(s, "")
}
