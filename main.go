package main

//// banco de dados https://www.youtube.com/watch?v=szOZ3p-5YIc para testar com a blockchain

import (
  "crypto/sha256"
  "encoding/hex"
  "errors"
  "fmt"
  "log"
  "math/rand"
  "time"
)

type NetworkPoS struct {
  ChainID    string
  Validators []*Node
  Txt        float64
  Tokens     []*Tokens
  Epic       []*Epic
}

type Epic struct {
  Nonce              int
  StartAt            string
  EndAt              string
  Blockchain         []*Block
  BlockchainHead     *Block
  TransactionsAmount int
}

type Tokens struct {
  Nft     []*Nft
  Balance float64
}

type Nft struct {
  Addres    string
  Balance   float64
  Serie     string
  Hash      string
  Timestamp string
}

type Node struct {
  Stake   int
  Address string
}

type Block struct {
  Timestamp     string
  PrevHash      string
  Hash          string
  ValidatorAddr string
  Nonce         int
  Transactions  []*Transaction
}

type Transaction struct {
  Sender       string  // Sender of the transaction
  Receiver     string  // Receiver of the transaction
  Amount       float64 // Amount of the transaction
  QuantityRate float64 // Amount of the transaction
  Rate         float64 //  Amount * txt = Rate => 10000 × 0,0001 = 1
  Timestamp    string
}

func (n Epic) GenerateNewBlock(Validator *Node) ([]*Block, *Block, error) {
  if err := n.ValidateBlockchain(); err != nil {
    Validator.Stake -= 10
    return n.Blockchain, n.BlockchainHead, err
  }

  currentTime := time.Now().UTC().String()

  newBlock := &Block{
    Nonce:         n.BlockchainHead.Nonce + 1,
    Timestamp:     currentTime,
    PrevHash:      n.BlockchainHead.Hash,
    Hash:          NewBlockHash(n.BlockchainHead),
    ValidatorAddr: Validator.Address,
  }

  if err := n.ValidateBlockCandidate(newBlock); err != nil {
    Validator.Stake -= 10
    return n.Blockchain, n.BlockchainHead, err
  } else {
    n.Blockchain = append(n.Blockchain, newBlock)
  }
  return n.Blockchain, newBlock, nil
}

func NewBlockHash(block *Block) string {
  blockInfo := block.Timestamp + block.PrevHash + block.Hash + block.ValidatorAddr
  return newHash(blockInfo)
}

func (n Epic) ValidateBlockchain() error {
  if len(n.Blockchain) <= 1 {
    return nil
  }

  currBlockIdx := len(n.Blockchain) - 1
  prevBlockIdx := len(n.Blockchain) - 2

  for prevBlockIdx >= 0 {
    currBlock := n.Blockchain[currBlockIdx]
    prevBlock := n.Blockchain[prevBlockIdx]
    if currBlock.PrevHash != prevBlock.Hash {
      return errors.New("blockchain has inconsistent hashes")
    }

    if currBlock.Timestamp <= prevBlock.Timestamp {
      return errors.New("blockchain has inconsistent timestamps")
    }

    if NewBlockHash(prevBlock) != currBlock.Hash {
      return errors.New("blockchain has inconsistent hash generation")
    }
    currBlockIdx--
    prevBlockIdx--
  }
  return nil
}

func (n Epic) ValidateBlockCandidate(newBlock *Block) error {
  if n.BlockchainHead.Hash != newBlock.PrevHash {
    return errors.New("blockchain HEAD hash is not equal to new block previous hash")
  }

  if n.BlockchainHead.Timestamp >= newBlock.Timestamp {
    return errors.New("blockchain HEAD timestamp is greater than or equal to new block timestamp")
  }

  if NewBlockHash(n.BlockchainHead) != newBlock.Hash {
    return errors.New("new block hash of blockchain HEAD does not equal new block hash")
  }
  return nil
}

func newHash(s string) string {
  h := sha256.New()
  h.Write([]byte(s))
  hashed := h.Sum(nil)
  return hex.EncodeToString(hashed)
}

func (n NetworkPoS) NewNode(stake int, Address string) []*Node {
  newNode := &Node{
    Stake:   stake,
    Address: Address,
  }
  n.Validators = append(n.Validators, newNode)
  return n.Validators
}

func randAddress() string {
  b := make([]byte, 32)
  _, _ = rand.Read(b)
  return fmt.Sprintf("%x", b)
}

func (n NetworkPoS) SelectWinner() (*Node, error) {
  var winnerPool []*Node
  totalStake := 0
  for _, node := range n.Validators {
    if node.Stake > 0 {
      winnerPool = append(winnerPool, node)
      totalStake += node.Stake
    }
  }
  if winnerPool == nil {
    return nil, errors.New("there are no nodes with stake in the network")
  }

  rand.Seed(time.Now().UnixNano())
  rand.Shuffle(len(winnerPool), func(i, j int) {
    winnerPool[i], winnerPool[j] = winnerPool[j], winnerPool[i]
  })

  winnerNumber := rand.Intn(totalStake)
  tmp := 0
  for _, node := range n.Validators {
    tmp += node.Stake
    if winnerNumber < tmp {
      return node, nil
    }
  }
  return nil, errors.New("a winner should have been picked but wasn't")
}

func main() {
  // set random seed
  rand.Seed(time.Now().UnixNano())

  // generate an initial PoS network including a blockchain with a genesis block.
  genesisTime := time.Now().UTC().String()
  networkPoS := &NetworkPoS{
    ChainID: "the-blockchain-bar-ledger",
    Txt:     0.00897,
    Epic: []*Epic{
      {
        Nonce:   0,
        StartAt: time.Now().UTC().String(),
        EndAt:   time.Now().Add(120 * time.Hour).UTC().String(),
        Blockchain: []*Block{
          {
            Nonce:         0,
            Timestamp:     genesisTime,
            PrevHash:      "",
            Hash:          newHash(genesisTime),
            ValidatorAddr: "",
          },
        },
      },
    },
  }

  networkPoS.Epic[0].BlockchainHead = networkPoS.Epic[0].Blockchain[0]

  // instantiate nodes to act as validators in our network
  networkPoS.Validators = networkPoS.NewNode(10, randAddress()+"-1")
  networkPoS.Validators = networkPoS.NewNode(100, randAddress()+"-2")
  networkPoS.Validators = networkPoS.NewNode(50, randAddress()+"-3")
  networkPoS.Validators = networkPoS.NewNode(30, randAddress()+"-4")
  networkPoS.Validators = networkPoS.NewNode(600, randAddress()+"-5")
  networkPoS.Validators = networkPoS.NewNode(760, randAddress()+"-6")
  networkPoS.Validators = networkPoS.NewNode(300, randAddress()+"-7")
  networkPoS.Validators = networkPoS.NewNode(140, randAddress()+"-8")
  networkPoS.Validators = networkPoS.NewNode(150, randAddress()+"-9")
  networkPoS.Validators = networkPoS.NewNode(596, randAddress()+"-10")

  networkPoS.Epic[0].Blockchain, networkPoS.Epic[0].BlockchainHead, _ = networkPoS.Epic[0].GenerateNewBlock(networkPoS.Validators[0])

  txt := networkPoS.Txt / 100
  amount := 1000.0
  amountTxt := amount * txt

  // build 5 additions to the blockchain
  for i := 1; i <= 10; i++ {

    fmt.Println("\nRound ", i, "\nAddress:", networkPoS.Epic[0].BlockchainHead.ValidatorAddr, "\nPrevHash Blockchain: ", networkPoS.Epic[0].BlockchainHead.PrevHash, "\nHash Blockchain: ", networkPoS.Epic[0].BlockchainHead.Hash, "\nNounce: ", networkPoS.Epic[0].BlockchainHead.Nonce)

    winner, err := networkPoS.SelectWinner()
    if err != nil {
      log.Fatal(err)
    }
    winner.Stake += 10
    networkPoS.Epic[0].Blockchain, networkPoS.Epic[0].BlockchainHead, err = networkPoS.Epic[0].GenerateNewBlock(winner)
    if err != nil {
      log.Fatal(err)
    }

    blockNew := networkPoS.Epic[0].BlockchainHead

    blockNew.Transactions = append(blockNew.Transactions, &Transaction{
      Amount:       amount * float64(i),
      QuantityRate: amount*float64(i) + amountTxt,
      Rate:         amountTxt,
      Receiver:     randAddress(),
      Sender:       randAddress(),
      Timestamp:    time.Now().UTC().String(),
    })

    fmt.Println("Sender: ", blockNew.Transactions[0].Sender, "\nReceiver", blockNew.Transactions[0].Receiver, "\nQuantityRate", blockNew.Transactions[0].QuantityRate)
    fmt.Println()

  }

  //networkPoS.PrintBlockchainInfo()
}
